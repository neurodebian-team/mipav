#!/bin/bash
#emacs: -*- mode: shell-script; c-basic-offset: 4; tab-width: 4; indent-tabs-mode: t -*- 
#ex: set sts=4 ts=4 sw=4 noet:
#-------------------------- =+- Shell script -+= --------------------------
#
# @file      update.sh
# @date      Fri Aug 20 15:46:15 2010
#
#
#  Yaroslav Halchenko                                            Dartmouth
#  web:     http://www.onerussian.com                              College
#  e-mail:  yoh@onerussian.com                              ICQ#: 60653192
#
# DESCRIPTION (NOTES):
#  Simple script to automate packaging for new releases of MIPAV binary
#  blobs
#
# COPYRIGHT: Yaroslav Halchenko 2010
#
# LICENSE: BSD-3
#
#-----------------\____________________________________/------------------

set -eu
echo "I: Given $# parameters: $@"

# Assure debian branch
git checkout debian

# Called with --upstream-version' <version> <file>
if [ "$1" = '--upstream-version' ] && [ $# == 3 ]; then
	uversion="$2"
	tarball="$3"
elif [ $# == 1 ]; then
	uversion=
	tarball="$1"
else
	echo >&2 "E: $0 was invoked in non-confirmant way"
	exit 1
fi

# Figure out full URL for the tarball
# Can't be used if tarball is already available
#url=`uscan --report --verbose --upstream-version 0 | grep -A1 -e 'Newer version available from' | tail -1 | sed -e 's/^ *//g'`
rurl=`uscan --report --verbose --upstream-version 0 \
	| sed -n -e '/Found the following matching hrefs:/,$p' \
	| grep 'frs/download.*tar\.gz' | tail -1 | sed -e 's/^ *//g'`
url="http://www.nitrc.org$rurl"
echo "I: Fresh tarball URL: $url"

# Check if filename of URL tarball matches the given one
fname=$(basename $tarball)
ufname=$(basename $url)			# name of the file at URL

# it could carry .orig and _ already
adj_fname=$(echo $fname | sed -e 's/\.orig//g' -e 's/mipav_/mipav-/g')
if [ "$fname" != "$ufname" ] && [ "$adj_fname" != "$ufname" ] ; then
	echo >&2 "E: Filename in $url is different from provided $fname and $adj_fname"
	exit 2
fi

# If version was not provided
if [ -z "$uversion" ]; then
	# figure out the version from the filename
	uversion=$(echo $fname | sed -e 's/mipav-\(.*\).tar.gz/\1/g')
fi

# check if filename is compliant to it
if [ ! "mipav-$uversion.tar.gz" = "$fname" ] && [ ! "mipav_$uversion.orig.tar.gz" = "$fname" ]; then
	echo >&2 "E: Provided version '$uversion' seems to differ from the ones in $fname"
	exit 1
fi
dversion=$(echo $uversion | perl -p -e 's/-(\d{4})-(\d{2})-(\d{2})/.$1$2$3/g')-1
echo "I: Upstream version: $uversion"
echo "I: Debian version: $dversion"

# Compute new sha512sum
sha512sum=$(sha512sum "$tarball" | awk '{print $1;}')
URL=$(dirname $url)				# base URL which changes

echo "I: SHA512SUM=$sha512sum"
echo "I: Adjusting debian/mipav.postinst"
sed -i -e "s,^URL=.*,URL=$URL,g" \
	-e "s,^FILE=.*,FILE=$ufname,g" \
	-e "s,^SHA512SUM=.*,SHA512SUM=$sha512sum,g" debian/mipav.postinst

echo "I: Generating new debian/changelog entry"
dch --noconf --newversion $dversion \
	-D UNRELEASED \
	"New upstream tarball for $uversion from JIST repository"
dch -e
